# Office

## 介绍
Microsoft Office、Visio和WPS Office的一些版本分享

## Microsoft Office

ProPlus:专业增强版; 

Pro：专业版; 

Std：标准版; 

HS：家庭学生版; 

1.    **Office 2021 ProPlus**  [推荐]

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/ProPlus2021Retail.img

2.   **Office 2021 Pro**  

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/Professional2021Retail.img

3.   **Office 2021 HS** 

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/HomeStudent2021Retail.img

4.   **Office 2019 ProPlus** 

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/ProPlus2019Retail.img

5.   **Office 2019 Pro** 

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/Professional2019Retail.img

6.   **Office 2019 HS**

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/HomeStudent2019Retail.img

7.    **Visio 2021 Pro**  [推荐]

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/VisioPro2021Retail.img

8.   **Visio 2021 Std**

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/VisioStd2021Retail.img

9.   **Visio 2019 Pro**

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/VisioPro2019Retail.img

10.   **Visio 2019 Std**

http://officecdn.microsoft.com/pr/492350f6-3a01-4f97-b9c0-c7c6ddf67d60/media/zh-cn/VisioStd2019Retail.img

11.   **Office 2024 ProPlus**

https://gitee.com/yu-bingxu/Office-Software/releases/download/0.0.0/MicrosoftOffice2024.zip

12.   **Microsoft 365**  

https://gitee.com/yu-bingxu/Office-Software/releases/download/0.0.0/Office365Setup.exe

1~10可将下载好的映像文件（类似于光盘或DVD）打开，并点击该文件夹下的setup.exe程序即可开始安装；

## Office激活方法

   建议通过Microsoft官网对Office产品进行购买和激活，可参考以下链接：
https://www.microsoftstore.com.cn/software/office

## WPS Office

1.  教育考试专用版WPS（考试专用，无广告，无需会员） - https://ncre.neea.edu.cn/xhtml1/report/1507/861-1.htm
2.  WPS特殊版本 - 链接：https://pan.baidu.com/s/1xQdbkHpc2UnMXtsvd1q62g?pwd=WPS1 提取码：WPS1
3.  WPS官网个人版本
