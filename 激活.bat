@echo off

color 0A
mode 120,30

:first
cls

echo =====Please choose your version=====
echo.
echo [A] Microsoft Office 2024 ProPlus
echo [B] Microsoft Office 2021 ProPlus
echo [C] Microsoft Office 2019 ProPlus
echo [D] Microsoft Visio 2024 Pro
echo [E] Microsoft Visio 2021 Pro
echo [F] Microsoft Visio 2019 Pro
echo [G] Microsoft Project 2024 Pro
echo [H] Microsoft Project 2021 Pro
echo [J] Microsoft Project 2019 Pro
echo.
echo [X] Windows 11/10 Pro
echo [Y] Windows 11/10 Edu
echo [Z] Windows 7 Pro
echo.
echo [0] Exit
echo.

set /p choice=Choose A-J or X-Z:

if /i "%choice%"=="A" (
goto A
) else if /i "%choice%"=="B" (
goto B
) else if /i "%choice%"=="C" (
goto C
) else if /i "%choice%"=="D" (
goto D
) else if /i "%choice%"=="E" (
goto E
) else if /i "%choice%"=="F" (
goto F
) else if /i "%choice%"=="G" (
goto G
) else if /i "%choice%"=="H" (
goto H
) else if /i "%choice%"=="J" (
goto J
) else if /i "%choice%"=="X" (
goto X
) else if /i "%choice%"=="Y" (
goto Y
) else if /i "%choice%"=="Z" (
goto Z
) else if /i "%choice%"=="0" (
goto end
) else (
    echo Error!
    pause
    goto first
)

:A
cd "C:\Program Files (x86)\Microsoft Office\Office*"
cd "C:\Program Files\Microsoft Office\Office*"
dir ospp.vbs
cscript ospp.vbs /inpkey:XJ2XN-FW8RK-P4HMP-DKDBV-GCVGB
cscript ospp.vbs /sethst:s1.kms.cx
cscript ospp.vbs /act
goto menu

:B
cd "C:\Program Files (x86)\Microsoft Office\Office16"
cd "C:\Program Files\Microsoft Office\Office16"
dir ospp.vbs
cscript ospp.vbs /inpkey:FXYTK-NJJ8C-GB6DW-3DYQT-6F7TH
cscript ospp.vbs /sethst:s1.kms.cx
cscript ospp.vbs /act
goto menu

:C
cd "C:\Program Files (x86)\Microsoft Office\Office16"
cd "C:\Program Files\Microsoft Office\Office16"
dir ospp.vbs
cscript ospp.vbs /inpkey:NMMKJ-6RK4F-KMJVX-8D9MJ-6MWKP
cscript ospp.vbs /sethst:s1.kms.cx
cscript ospp.vbs /act
goto menu

:D
cd "C:\Program Files (x86)\Microsoft Office\Office*"
cd "C:\Program Files\Microsoft Office\Office*"
dir ospp.vbs
cscript ospp.vbs /inpkey:B7TN8-FJ8V3-7QYCP-HQPMV-YY89G
cscript ospp.vbs /sethst:s1.kms.cx
cscript ospp.vbs /act
goto menu

:E
cd "C:\Program Files (x86)\Microsoft Office\Office16"
cd "C:\Program Files\Microsoft Office\Office16"
dir ospp.vbs
cscript ospp.vbs /inpkey:KNH8D-FGHT4-T8RK3-CTDYJ-K2HT4
cscript ospp.vbs /sethst:s1.kms.cx
cscript ospp.vbs /act
goto menu

:F
cd "C:\Program Files (x86)\Microsoft Office\Office16"
cd "C:\Program Files\Microsoft Office\Office16"
dir ospp.vbs
cscript ospp.vbs /inpkey:9BGNQ-K37YR-RQHF2-38RQ3-7VCBB
cscript ospp.vbs /sethst:s1.kms.cx
cscript ospp.vbs /act
goto menu

:G
cd "C:\Program Files (x86)\Microsoft Office\Office*"
cd "C:\Program Files\Microsoft Office\Office*"
dir ospp.vbs
cscript ospp.vbs /inpkey:FQQ23-N4YCY-73HQ3-FM9WC-76HF4
cscript ospp.vbs /sethst:s1.kms.cx
cscript ospp.vbs /act
goto menu

:H
cd "C:\Program Files (x86)\Microsoft Office\Office16"
cd "C:\Program Files\Microsoft Office\Office16"
dir ospp.vbs
cscript ospp.vbs /inpkey:FTNWT-C6WBT-8HMGF-K9PRX-QV9H8
cscript ospp.vbs /sethst:s1.kms.cx
cscript ospp.vbs /act
goto menu

:J
cd "C:\Program Files (x86)\Microsoft Office\Office16"
cd "C:\Program Files\Microsoft Office\Office16"
dir ospp.vbs
cscript ospp.vbs /inpkey:B4NPR-3FKK7-T2MBV-FRQ4W-PKD2B
cscript ospp.vbs /sethst:s1.kms.cx
cscript ospp.vbs /act
goto menu

:X
slmgr /skms s1.kms.cx
slmgr /ipk W269N-WFGWX-YVC9B-4J6C9-T83GX
slmgr /ato
slmgr /xpr
goto menu

:Y
slmgr /skms s1.kms.cx
slmgr /ipk NW6C2-QMPVW-D7KKK-3GKT6-VCFB2
slmgr /ato
slmgr /xpr
goto menu

:Z
slmgr /skms s1.kms.cx
slmgr /ipk FJ82H-XT6CR-J8D7P-XQJJ2-GPDD4
slmgr /ato
slmgr /xpr
goto menu

:end
exit

:menu
echo.
echo.
pause
goto first
